module modernc.org/b

go 1.21

require (
	modernc.org/fileutil v1.3.0
	modernc.org/mathutil v1.7.1
	modernc.org/strutil v1.2.1
)

require github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
